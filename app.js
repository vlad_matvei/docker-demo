'use strict';

var express = require('express'),
    routes = require('./routes'),
    bodyParser = require('body-parser'),
    methodOverride = require('method-override'),

    app = express();

app.set('view engine', 'jade');
app.use(bodyParser.json());
app.use(methodOverride('_method'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(__dirname + '/public'));
app.get('/', routes.index);

app.listen(process.env.PORT || 3000);

module.exports = app;